FROM      ubuntu:18.04
MAINTAINER Robert Terbach <robert.terbach@googlemail.com>

RUN apt-get update \
    && apt-get upgrade -y --force-yes \
    && apt-get install -y --force-yes \
    wget \
    gnupg \
    npm \
    locales > /dev/null

RUN echo "en_US UTF-8" > /etc/locale.gen
RUN locale-gen en_US.UTF-8
RUN export LANG=en_US.UTF-8
RUN export LANGUAGE=en_US:en
RUN export LC_ALL=en_US.UTF-8
RUN update-locale LC_ALL=en_US.UTF-8
RUN which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
RUN mkdir -p ~/.ssh
RUN wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && dpkg -i erlang-solutions_1.0_all.deb
RUN apt-get update -qq && apt-get install -y -qq esl-erlang elixir
RUN npm install webpack
RUN mix local.rebar --force
RUN mix local.hex --force
RUN echo locale
